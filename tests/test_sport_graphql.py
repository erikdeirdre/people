""" Unit Test Module for Graphql """
import sys
from os.path import (join, abspath, dirname)
import json 
import unittest
import pytest
from graphene.test import Client
from app.schema import SCHEMA
from testhelper.testhelper import TestHelper


@pytest.mark.usefixtures("init_database")
class TestSportGraphGL(unittest.TestCase):
    """Test Suite for testing Sport GraphQL"""
    dir_name = join(abspath(dirname(__file__)), 'files')
    client = Client(SCHEMA)

    @pytest.mark.order(1)
    def test_sport_list(self):
        """Execute sport listing test"""
        test_data = TestHelper(self.dir_name,
                                sys._getframe(  ).f_code.co_name)
        test_data.load_files()

        executed = self.client.execute(test_data.get_send_request())

        self.assertDictEqual(executed['data'],
                             test_data.get_expected_result()['data'])

    @pytest.mark.order(2)
    def test_sport_create(self):
        """Execute sport create test"""
        test_data = TestHelper(self.dir_name,
                                  sys._getframe(  ).f_code.co_name)
        test_data.load_files()

        executed = self.client.execute(
            test_data.get_send_request(),
            variable_values=test_data.get_variables())

        self.assertDictEqual(executed['data'],
                             test_data.get_expected_result()['data'])

    @pytest.mark.order(3)
    def test_sport_create_exists(self):
        """Execute sport create exists test"""
        test_data = TestHelper(self.dir_name,
                                  sys._getframe(  ).f_code.co_name)
        test_data.load_files()

        executed = self.client.execute(
            test_data.get_send_request(),
            variable_values=test_data.get_variables())

        self.assertDictEqual(executed['data'],
                             test_data.get_expected_result()['data'])

    def test_sport_update(self):
        """Execute sport update test"""
        test_data = TestHelper(self.dir_name,
                                  sys._getframe(  ).f_code.co_name)
        test_data.load_files()
        get_id = "{allSports(filters:{description: \"Baseball\"}) {edges {node {id}}}}"

        executed = self.client.execute(get_id)
        sport_id = executed['data']['allSports']['edges'][0]['node']['id']
        sport_vars = {"sport": {"description": "SomeSport", "active": True,
                "id": sport_id}}
        executed = self.client.execute(
            test_data.get_send_request(), variable_values=sport_vars)
        self.assertEqual(executed['data']['updateSport']['ok'], True)
        self.assertEqual(executed['data']['updateSport']['message'],
                        "Successfully Updated")   
    
if __name__ == '__main__':
    unittest.main()
