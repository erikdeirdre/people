""" Unit Test Module for Graphql """
import sys
from os.path import (join, abspath, dirname)
import json 
import unittest
import pytest
from graphene.test import Client
from app.schema import SCHEMA
from testhelper.testhelper import TestHelper


@pytest.mark.usefixtures("init_database")
class TestGameGraphGL(unittest.TestCase):
    """Test Suite for testing Game GraphQL"""
    dir_name = join(abspath(dirname(__file__)), 'files')
    client = Client(SCHEMA)

    @pytest.mark.order(1)
    def test_game_list(self):
        """Execute game listing test"""
        test_data = TestHelper(self.dir_name,
                                sys._getframe(  ).f_code.co_name)
        test_data.load_files()

        executed = self.client.execute(test_data.get_send_request())

        self.assertDictEqual(executed['data'],
                             test_data.get_expected_result()['data'])

    def test_game_create(self):
        """Execute game create test"""
        test_data = TestHelper(self.dir_name,
                                  sys._getframe(  ).f_code.co_name)
        test_data.load_files()

        executed = self.client.execute(
            test_data.get_send_request(),
            variable_values=test_data.get_variables())

        self.assertDictEqual(executed['data'],
                             test_data.get_expected_result()['data'])

    def test_game_create_exists(self):
        """Execute game create exists test"""
        test_data = TestHelper(self.dir_name,
                                  sys._getframe(  ).f_code.co_name)
        test_data.load_files()

        executed = self.client.execute(
            test_data.get_send_request())

        self.assertDictEqual(executed['data'],
                             test_data.get_expected_result()['data'])

    def test_game_create_bad_team_venue(self):
        """Execute game create exists test"""
        test_data = TestHelper(self.dir_name,
                                  sys._getframe(  ).f_code.co_name)
        test_data.load_files()

        executed = self.client.execute(
            test_data.get_send_request(),
            variable_values=test_data.get_variables())

        self.assertDictEqual(executed['data'],
                             test_data.get_expected_result()['data'])

    def test_game_update(self):
        """Execute game update test"""
        test_data = TestHelper(self.dir_name,
                                  sys._getframe(  ).f_code.co_name)
        test_data.load_files()

        executed = self.client.execute(
            test_data.get_send_request(),
            variable_values=test_data.get_variables())

        self.assertDictEqual(executed['data'],
                             test_data.get_expected_result()['data']) 

    def test_game_update_invalid_score(self):
        """Execute game update test"""
        test_data = TestHelper(self.dir_name,
                                  sys._getframe(  ).f_code.co_name)
        test_data.load_files()

        executed = self.client.execute(
            test_data.get_send_request(),
            variable_values=test_data.get_variables())

        self.assertDictEqual(executed['data'],
                             test_data.get_expected_result()['data']) 

if __name__ == '__main__':
    unittest.main()
