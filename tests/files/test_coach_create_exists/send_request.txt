mutation AddCoach($coach: CoachInput!) {
  createCoach(coachData: $coach) {
    ok
    message
  }
}
