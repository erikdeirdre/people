mutation addTeam($team: CreateTeamInput!) {
  createTeam(teamData: $team) {
    ok
    message
    team {
      description
    }
  }
}
