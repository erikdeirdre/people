mutation UpdateCoach($coach: CoachInput!, $sport:SportsInput) {
  updateCoach(coachData: $coach, sportData: $sport) {
    ok
    message
  }
}
