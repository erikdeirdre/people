mutation UpdateReferee($referee: RefereeInput!, $sport:SportsInput) {
  updateReferee(refereeData: $referee, sportData: $sport) {
    ok
    message
  }
}
