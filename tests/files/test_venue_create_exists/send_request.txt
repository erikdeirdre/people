mutation AddVenue($venue: VenueInput!) {
  createVenue(venueData: $venue) {
    ok
    message
  }
}
