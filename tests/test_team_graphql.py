""" Unit Test Module for Graphql """
import sys
from os.path import (join, abspath, dirname)
from time import sleep
import unittest
import pytest
from graphene.test import Client
from testhelper.testhelper import TestHelper
from app.schema import SCHEMA


@pytest.mark.usefixtures("init_database")
class TestTeamGraphGL(unittest.TestCase):
    """Test Suite for testing Sport GraphQL"""
    dir_name = join(abspath(dirname(__file__)), 'files')
    client = Client(SCHEMA)
    last_id = None

    @pytest.mark.order(1)
    def test_team_list(self):
        """Execute team listing test"""
        test_data = TestHelper(self.dir_name,
                                  sys._getframe(  ).f_code.co_name)

        test_data.load_files()

        executed = self.client.execute(test_data.get_send_request())
        self.assertDictEqual(executed['data'],
                             test_data.get_expected_result()['data'])

    @pytest.mark.order(2)
    def test_team_create(self):
        """Execute team create test"""
        test_data = TestHelper(self.dir_name,
                                  sys._getframe(  ).f_code.co_name)

        test_data.load_files()

        executed = self.client.execute(
            test_data.get_send_request(),
            variable_values=test_data.get_variables())

        self.assertDictEqual(executed['data'],
                             test_data.get_expected_result()['data'])

    @pytest.mark.order(3)
    def test_team_create_exists(self):
        """Execute team create test"""
        test_data = TestHelper(self.dir_name,
                                  sys._getframe(  ).f_code.co_name)

        test_data.load_files()

        executed = self.client.execute(
            test_data.get_send_request(),
            variable_values=test_data.get_variables())

        sleep(1)
        self.assertDictEqual(executed['data'],
                             test_data.get_expected_result()['data'])

    def test_team_update(self):
        """Execute team update test"""
        test_data = TestHelper(self.dir_name,
                                  sys._getframe(  ).f_code.co_name)
        test_data.load_files()
        get_id = "{allTeams(filters:{description: \"Bruins\"}) {edges {node {id}}}}"

        executed = self.client.execute(get_id)
        team_id = executed['data']['allTeams']['edges'][0]['node']['id']
        team_vars = {"team": {"description": "SomeTeam", "active": True,
                "id": team_id}}
        executed = self.client.execute(
            test_data.get_send_request(), variable_values=team_vars)
        self.assertEqual(executed['data']['updateTeam']['ok'], True)
        self.assertEqual(executed['data']['updateTeam']['message'],
                        "Successfully Updated")   

if __name__ == '__main__':
    unittest.main()
