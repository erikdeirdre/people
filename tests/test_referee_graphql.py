""" Unit Test Module for Graphql """
import sys
from os.path import (join, abspath, dirname)
import unittest
import pytest
from graphene.test import Client
from testhelper.testhelper import TestHelper
from app.schema import SCHEMA

TestHelper.__test__ = False


@pytest.mark.usefixtures("init_database")
class TestRefereeGraphGL(unittest.TestCase):
    """Test Suite for testing Referee GraphQL"""
    dir_name = join(abspath(dirname(__file__)), 'files')
    client = Client(SCHEMA)

    @pytest.mark.order(1)
    def test_referee_list(self):
        """Execute referee query test"""
        test_data = TestHelper(self.dir_name,
                              sys._getframe(  ).f_code.co_name)
        test_data.load_files()

        executed = self.client.execute(test_data.get_send_request())

        self.assertDictEqual(executed['data'],
                         test_data.get_expected_result()['data'])


    def test_referee_exists(self):
        """Execute referee exists test"""
        test_data = TestHelper(self.dir_name,
                              sys._getframe(  ).f_code.co_name)
        test_data.load_files()

        executed = self.client.execute(test_data.get_send_request())

        self.assertDictEqual(executed['data'],
                         test_data.get_expected_result()['data'])

    def test_referee_create(self):
        """Execute referee create test"""
        test_data = TestHelper(self.dir_name,
                              sys._getframe(  ).f_code.co_name)
        test_data.load_files()
    
        executed = self.client.execute(
            test_data.get_send_request(), variable_values=test_data.get_variables())
        self.assertEqual(executed['data']['createReferee']['ok'], True)
        self.assertEqual(executed['data']['createReferee']['message'][0],
            "Successfully Created")


    def test_referee_create_no_level_sport(self):
        """Execute referee create no level or sport test"""
        test_data = TestHelper(self.dir_name,
                                  sys._getframe(  ).f_code.co_name)
        test_data.load_files()

        executed = self.client.execute(
            test_data.get_send_request(),
            variable_values=test_data.get_variables())

        self.assertDictEqual(executed['data'],
                             test_data.get_expected_result()['data'])


    def test_referee_create_exists(self):
        """Execute referee create exists test"""
        test_data = TestHelper(self.dir_name,
                                  sys._getframe(  ).f_code.co_name)
        test_data.load_files()

        executed = self.client.execute(
            test_data.get_send_request(),
            variable_values=test_data.get_variables())

        self.assertDictEqual(executed['data'],
                             test_data.get_expected_result()['data'])

    def test_referee_update(self):
        test_data = TestHelper(self.dir_name,
                              sys._getframe(  ).f_code.co_name)
        test_data.load_files()

        executed = self.client.execute(
            test_data.get_send_request(),
            variable_values=test_data.get_variables())

        self.assertDictEqual(executed['data'],
                             test_data.get_expected_result()['data'])

    def test_referee_update_invalid_email(self):
        test_data = TestHelper(self.dir_name,
                              sys._getframe(  ).f_code.co_name)
        test_data.load_files()

        executed = self.client.execute(
            test_data.get_send_request(),
            variable_values=test_data.get_variables())

        self.assertDictEqual(executed['data'],
                             test_data.get_expected_result()['data'])

    def test_referee_update_invalid_level_sport(self):
        test_data = TestHelper(self.dir_name,
                              sys._getframe(  ).f_code.co_name)
        test_data.load_files()

        executed = self.client.execute(
            test_data.get_send_request(),
            variable_values=test_data.get_variables())

        self.assertDictEqual(executed['data'],
                             test_data.get_expected_result()['data'])

if __name__ == '__main__':
    unittest.main()
