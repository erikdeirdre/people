# Official language image. Look for the different tagged releases at:
# https://hub.docker.com/r/library/python/tags/
image: "python:3.10"

services:
  - mysql:latest

# Change pip's cache directory to be inside the project directory since we can
# only cache local items.
variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
  GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
  MYSQL_DATABASE: people_test
  MYSQL_ROOT_PASSWORD: mysql
  SQLALCHEMY_URI: "mysql://root:mysql@mysql/people_test"
  USPS_URL: $USPS_URL
  USPS_USERID: $USPS_USERID
  SSH_PRIVATE_KEY: $SSH_PRIVATE_KEY
  SSH_KNOWN_HOSTS: $SSH_KNOWN_HOSTS
  DEPLOY_HOST: $DEPLOY_HOST

stages:
  - run_tests
  - analyze_tests
  - deploy_production

# Pip's cache doesn't store the python packages
# https://pip.pypa.io/en/stable/reference/pip_install/#caching
#
# If you want to also cache the installed packages, you have to install
# them in a virtualenv and cache it as well.
cache:
  paths:
    - .cache/pip
    - venv/

before_script:
  - pip install virtualenv
  - virtualenv venv
  - source venv/bin/activate
  - pip install -r requirements.txt
  - pip install -r tests/requirements.txt

test:
  stage: run_tests
  script:
    - flask db upgrade
    - coverage run -m pytest --junitxml=coverage.xml
    - coverage xml

  artifacts:
    when: always
    paths:
      - coverage.xml
    expire_in: 30 min

sonarcloud-check:
  stage: analyze_tests
  before_script: 
    - ''
  image:
    name: sonarsource/sonar-scanner-cli:latest
    entrypoint: [""]
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - find ./ -name coverage.xml -print
    - cat coverage.xml
    - sonar-scanner -Dsonar.qualitygate.wait=true -Dsonar.coverageReportPath=coverage.xml
  dependencies:
    - test

deploy_production:
  stage: deploy_production
  rules:
  - if: '$CI_COMMIT_BRANCH == "main"'
  before_script:
  - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
  - eval $(ssh-agent -s)
  - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
  - mkdir -p ~/.ssh
  - chmod 700 ~/.ssh
  - echo "$SSH_KNOWN_HOSTS" > ~/.ssh/known_hosts
  - chmod 644 ~/.ssh/known_hosts
  script:
    - ssh $DEPLOY_HOST "sudo /root/update_people_backend.sh ${CI_COMMIT_SHA}"
  environment:
    name: production
    url: https://$DEPLOY_HOST
  dependencies:
    - test
