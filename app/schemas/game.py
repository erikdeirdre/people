""" Graphql Game Schema Module """
from email import message
import logging
from graphene import (InputField, ID, Int, InputObjectType, Node, Boolean,
                      List, Field, String, Mutation, Connection, DateTime)
from graphene_sqlalchemy import SQLAlchemyObjectType
from app.filters import FilterConnectionField

from app import (DB)
from app.database import (Game as GameModel, Address as AddressModel,
                          Team as TeamModel, Venue as VenueModel,
                          GameStatus)
from .helpers import (TotalCount, Teams as TeamField,
                      Venue as VenueField, input_to_dictionary)

logger = logging.getLogger(__name__)

# Need to check:
# date
# status
# score can only be set when status is complete (2)

def check_game_data(game_data):
    message = []
    status = True

    team_db = DB.session.query(TeamModel).filter_by(
                id=game_data['home_team_id'],active=True).first()
    if not team_db:
        status = False
        message.append("Home Team not found")

    team_db = DB.session.query(TeamModel).filter_by(
                id=game_data['away_team_id'],active=True).first()
    if not team_db:
        status = False
        message.append("Away Team not found")

    venue_db = DB.session.query(VenueModel).filter_by(
                id=game_data['venue_id'],active=True).first()
    if not venue_db:
        status = False
        message.append("Venue not found")

    if game_data['home_team_id'] == game_data['away_team_id']:
        status = False
        message.append("Away and Home Teams must be different.")

    if game_data['status'] != "COMPLETED" and \
        (game_data['home_score'] > 0 or game_data['away_score'] > 0):
        status = False
        message.append("Game scores allowed for completed games.")

    return status, message


class GameNode(SQLAlchemyObjectType):
    """ Game Node """
    class Meta:
        """ Game Node """
        model = GameModel
        interfaces = (Node,)
        connection_field_factory = FilterConnectionField.factory

    venue = Field(VenueField)

    def resolve_venue(self, info):
        ''' venue resolver '''
        venue = {
            'id': None, 'name': None, 'address1': None, 'address2': None,
            'city': None, 'state': None, 'zip_code': None
        }
        result = DB.session.query(GameModel).join(VenueModel).join(AddressModel).filter(
            VenueModel.id == self.venue_id)
        for row in result:
            venue = {
                'id': self.venue_id,
                'name': row.venue.name,
                'active': row.venue.active,
                'address1': row.venue.address.address1,
                'address2': row.venue.address.address2,
                'city': row.venue.address.city,
                'state': row.venue.address.state,
                'zip_code': row.venue.address.zip_code
            }

        return venue


class GameConnection(Connection):
    """Game Graphql Query output"""
    class Meta:
        """Game Graphql Query output"""
        node = GameNode
        interfaces = (TotalCount,)


class GameAttribute:
    """Game Input Template """
    game_dt = DateTime()
    home_team_id = Field(TeamField)
    away_team_id = Field(TeamField)
    home_score = Int()
    away_score = Int()
    status = String()
    venue_id = Field(VenueField)

#
class CreateGameInput(InputObjectType, GameAttribute):
    """Create Game Input fields derived from GameAttribute"""
    home_team_id = InputField(ID, required=True)
    away_team_id = InputField(ID, required=True)
    venue_id = InputField(ID, required=True)
    home_score = InputField(Int)
    away_score = InputField(Int)
    status = InputField(String)


class UpdateGameInput(InputObjectType, GameAttribute):
    """Create Game Input fields derived from GameAttribute"""
    id = InputField(ID, required=True)
    home_team_id = InputField(ID, required=True)
    away_team_id = InputField(ID, required=True)
    venue_id = InputField(ID, required=True)
    home_score = InputField(Int)
    away_score = InputField(Int)
    status = InputField(String)

class CreateGame(Mutation):
    """Create Game Graphql"""
    game = Field(lambda: GameNode,
                  description="Game created by this mutation.")
    ok = Boolean()
    message = List(String)

    class Arguments:
        """Arguments for Create Game"""
        game_data = CreateGameInput(required=True)

    def mutate(self, info, game_data):
        """Create Game Graphql"""
        data = input_to_dictionary(game_data)
        ok = True
        message = []
        game = GameModel(**data)
 
        try:
            game_db = DB.session.query(GameModel).filter_by(
                game_dt=data['game_dt'],
                venue_id=data['venue_id'],
                home_team_id=data['home_team_id'],
                away_team_id=data['away_team_id']
            ).first()
            if game_db:
                message.append("Game already exists in the database")
                ok = False
                game = game_db
                logger.debug(f"Game {game_db.id} exists in the database")
            else:
                data['status'] = 1
                data['home_score'] = 0
                data['away_score'] = 0
                game_status, game_message = check_game_data(data)
                if game_status:
                    DB.session.add(game)
                    DB.session.commit()
                    message.append('Successfully Added Game')
                else:
                    ok = False
                    message = game_message
        except Exception as e:
            ok = False
            message.append("Failed to add Game")
            logger.error(f"Failed to add {game.game_dt}. error: {e}")

        return CreateGame(game=game, ok=ok, message=message)


class UpdateGame(Mutation):
    """Update Game Graphql"""
    game = Field(lambda: GameNode,
                  description="Game updated by this mutation.")
    ok = Boolean()
    message = List(String)

    class Arguments:
        """Arguments for Update Game"""
        game_data = UpdateGameInput(required=True)

    def mutate(self, info, game_data):
        """Update Game Graphql"""
        data = input_to_dictionary(game_data)
        ok = True
        message = []

        game = GameModel(**data)

        try:
            game_db = DB.session.query(GameModel).filter_by(id=data['id']).first()        
            if game_db: 
                game_status, game_message = check_game_data(data)
                if game_status:           
                    game = data
                    DB.session.commit()
                    message.append("Successfully Updated Game")
                else:
                    ok = False
                    message = game_message
            else:
                message.append("Game doesn't exist")
                ok = False
        except Exception as e:
            ok = False
            message.append("Failed to add Game")
            logger.error(f"Failed to add {data['id']}. error: {e}")
        
        return UpdateGame(game=game, ok=ok, message=message)
