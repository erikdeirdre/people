""" Graphql Level Schema Module """
import logging
from graphene import (String, Boolean, ID, InputObjectType, List,
                      Field, Mutation, Connection, Node)
from graphene_sqlalchemy import SQLAlchemyObjectType
from app.filters import FilterConnectionField

from helpers.utils import (input_to_dictionary)
from app import DB
from app.database import (Level as LevelModel)
from .helpers import (TotalCount)

logger = logging.getLogger(__name__)


class LevelNode(SQLAlchemyObjectType):
    """ Level Node """
    class Meta:
        """ Level Node """
        model = LevelModel
        interfaces = (Node,)
        connection_field_factory = FilterConnectionField.factory


class LevelConnection(Connection):
    """Level Graphql Query output"""
    class Meta:
        """Level Graphql Query output"""
        node = LevelNode
        interfaces = (TotalCount,)
