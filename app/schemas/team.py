""" Graphql Team Schema Module """
import logging
from graphene import (String, Boolean, ID, InputObjectType,
                      Field, Mutation, Interface,
                      Connection, Node)
from graphene_sqlalchemy import SQLAlchemyObjectType
from app.filters import FilterConnectionField

from helpers.utils import (input_to_dictionary)
from app import DB
from app.database import (Team as TeamModel)
from .helpers import TotalCount

logger = logging.getLogger(__name__)


class TeamNode(SQLAlchemyObjectType):
    """ Team Node """
    class Meta:
        """ Team Node """
        model = TeamModel
        interfaces = (Node,)
        connection_field_factory = FilterConnectionField.factory


class TeamConnection(Connection):
    """ Team Connection """
    class Meta:
        """ Team Connection """
        node = TeamNode
        interfaces = (TotalCount,)


class TeamAttribute:
    """ Team Graphql Attributes """
    description = String()
    active = Boolean()


class CreateTeamInput(InputObjectType, TeamAttribute):
    """Create Team Input fields derived from TeamAttribute"""


class CreateTeam(Mutation):
    """Create Team Graphql"""
    team = Field(lambda: TeamNode,
                 description="Team created by this mutation.")
    ok = Boolean()
    message = String()

    class Arguments:
        """Arguments for Create Team"""
        team_data = CreateTeamInput(required=True)

    def mutate(self, info, team_data=None):
        """Mutation method for Create Team"""
        ok = True
        message = "Successfully Added"
        data = input_to_dictionary(team_data)

        try:
            team = TeamModel(**data)
            team_db = DB.session.query(TeamModel).filter_by(
                description=data['description']).first()
            if team_db:
                message = f"{data['description']} exists in the database"
                ok = False
                team = team_db
                logger.debug(f"Team: {data['description']} exists in the database")
            else:
                DB.session.add(team)
                DB.session.commit()
                logger.debug(f"Results: {team}, ok: {ok}, message: {message}")
        except Exception as e:
            ok = False
            message = f"Failed to add {team}"
            logger.error(f"Failed to add {team}. error: {e}")

        return CreateTeam(team=team, ok=ok, message=message)


class UpdateTeamInput(InputObjectType, TeamAttribute):
    """Update Team Input fields derived from TeamAttribute"""
    id = ID(required=True, description="Global Id of the Team.")


class UpdateTeam(Mutation):
    """Update Team Graphql"""
    team = Field(lambda: TeamNode,
                 description="Team updated by this mutation.")
    ok = Boolean()
    message = String()

    class Arguments:
        """Arguments for Update Team"""
        team_data = UpdateTeamInput(required=True)

    def mutate(self, info, team_data):
        """Mutation method for Update Team"""
        data = input_to_dictionary(team_data)
        """Mutation method for Create Team"""
        ok = True
        message = "Successfully Updated"

        try:
            team = DB.session.query(TeamModel).filter_by(id=data['id'])
            team.update(data)
            DB.session.commit()
            logger.debug(f"Successfully updated, {team}")
        except Exception as e:
            ok = False
            message = f"Error updating {team}"
            logger.error(f"Error updating {team}. error: {e}")

        return UpdateTeam(team=team, ok=ok, message=message)
