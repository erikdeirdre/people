""" Graphql Field Schema Module """
import logging
from graphene import (String, Boolean, ID, InputObjectType, Node,
                      Field, Mutation, Connection, List, InputField)
from graphene_sqlalchemy import SQLAlchemyObjectType
from app.filters import FilterConnectionField

from app import (DB)
from app.database import (Venue as VenueModel, Address)
from .helpers import (TotalCount, Address as AddressField, VenueAttribute,
                      VenueInput, AddressInput, input_to_dictionary,
                      process_address)

logger = logging.getLogger(__name__)


class VenueNode(SQLAlchemyObjectType):
    """ Venue Node """
    class Meta:
        """ Venue Node """
        model = VenueModel
        interfaces = (Node,)
        connection_field_factory = FilterConnectionField.factory

    address = Field(AddressField)

    def resolve_address(self, info):
        ''' address resolver '''
        address = {
            'id': None, 'address1': None, 'address2': None,
            'city': None, 'state': None, 'zip_code': None
        }
        result = DB.session.query(VenueModel).join(Address).filter(
            Address.id == self.address_id)
        for row in result:
            address = {
                'id': self.address_id,
                'address1': row.address.address1,
                'address2': row.address.address2,
                'city': row.address.city,
                'state': row.address.state,
                'zip_code': row.address.zip_code
            }

        return address


class VenueConnection(Connection):
    """Venue Graphql Query output"""
    class Meta:
        """Venue Graphql Query output"""
        node = VenueNode
        interfaces = (TotalCount,)


class UpdateVenueInput(InputObjectType, VenueAttribute):
    """Venue Input fields derived from VenueAttribute"""
    id = InputField(ID, required=True)
    address = InputField(AddressInput)

class CreateVenue(Mutation):
    """Create Venue Graphql"""
    venue = Field(lambda: VenueNode,
                  description="Venue created by this mutation.")
    ok = Boolean()
    message = List(String)

    class Arguments:
        """Arguments for Create Field"""
        venue_data = VenueInput(required=True)

    def mutate(self, info, venue_data):
        """Create Venue Graphql"""
        data = input_to_dictionary(venue_data)
        ok = True
        message = []
        venue = VenueModel()

        try:
            venue_db = DB.session.query(VenueModel).filter_by(
                name=data['name']).first()
            if venue_db:
                message.append(f"{data['name']} exists in the database")
                ok = False
                venue = venue_db
                logger.debug(f"Field {data['name']} exists in the database")
            else:
                venue.name = data["name"]
                venue.active = data["active"]
                address = process_address(data['address'])
                if address['status']:
                    venue.address_id = address["id"]
                    DB.session.add(venue)
                    DB.session.commit()
                    message.append('Successfully added Venue')
                else:
                    ok = False
                    message.append("Failed to add Venue, missing address information")
                    message += address['message']
        except Exception as e:
            ok = False
            message.append("Failed to add venue")
            logger.error(f"Failed to add {venue.name}. error: {e}")

        return CreateVenue(venue=venue, ok=ok, message=message)


class UpdateVenue(Mutation):
    """Update Venue Graphql"""
    venue = Field(lambda: VenueNode,
                  description="Venue updated by this mutation.")
    ok = Boolean()
    message = List(String)


    class Arguments:
        """Arguments for Update Venue"""
        venue_data = UpdateVenueInput(required=True)

    def mutate(self, info, venue_data):
        """Update Field Graphql"""
        data = input_to_dictionary(venue_data)
        ok = True
        message = []

        try:
            venue_db = DB.session.query(VenueModel).filter_by(id=data['id']).first()
            if venue_db:
                venue = data
                if "address" in data:
                    address = process_address(data['address'])
                    if address['status']:
                        venue.address_id = address["id"]         
                    else:
                        ok = False
                        message.append("Failed to process address")          
                DB.session.commit()
                logger.debug("Successfully Updated")
                message.append("Successfully Updated")
            else:
                message.append(f"Venue , {data['name']} doesn't exist")
                ok = False
        except Exception as e:
            ok = False
            message.append(f"Failed to add {venue.name}")
            logger.error(f"Failed to add {venue.name}. error: {e}")
        return UpdateVenue(venue=venue, ok=ok, message=message)
