""" Graphql Coach Schema Module """
import logging
from graphene import (String, Boolean, ID, InputObjectType, Node,
                      Field, Mutation, Connection, List, InputField)
from graphene_sqlalchemy import SQLAlchemyObjectType
from app.filters import FilterConnectionField

from helpers.utils import (input_to_dictionary)
from app import (DB)
from app.database import (Coach as CoachModel, Sport, CoachSport,
                          CoachTeam, Team, Level)
from .helpers import (TotalCount, Sports, Teams, SportsInput, TeamsInput,
                      process_sport)

logger = logging.getLogger(__name__)


class CoachNode(SQLAlchemyObjectType):
    """ Coach Node """
    class Meta:
        """ Coach Node """
        model = CoachModel
        interfaces = (Node,)
        connection_field_factory = FilterConnectionField.factory

    sport = List(Sports)
    team = List(Teams)

    def resolve_sport(self, info):
        ''' sport resolver '''
        sports = []

        results = DB.session.query(CoachSport, Sport).join(Sport).filter(
            CoachSport.coach_id == self.id)
        for row in results:
            level = None
            level_date = None
            level_years = 0
            description = None
            active = False
            level_id = None
            sport_id = None
            if row.CoachSport.level:
                level = row.CoachSport.level.description
                level_date = row.CoachSport.level_date
                level_years = row.CoachSport.years_level
                description = row.Sport.description
                active = row.CoachSport.active
                level_id = row.CoachSport.level_id
                sport_id = row.CoachSport.sport_id

            sports.append({'id': sport_id,
                           'description': description,
                           'active': active,
                           'level_id': level_id,
                           'level': level,
                           'level_date': level_date,
                           'level_years': level_years})

        return sports

    def resolve_team(self, info):
        ''' team resolver '''
        teams = []
        results = DB.session.query(CoachTeam, Team).join(Team).filter(
            CoachTeam.coach_id == self.id)
        for row in results:
            join_date = None
            join_years = 0
            active = False
            description = None

            if row.CoachTeam.active:
                join_date = row.CoachTeam.join_date
                join_years = row.CoachTeam.years_active
                active = row.CoachTeam.active
                description = row.Team.description
            teams.append({'id': row.Team.id,
                          'description': description,
                          'active': active,
                          'join_date': join_date,
                          'join_years': join_years})
        return teams

class CoachConnection(Connection):
    """Coach Graphql Query output"""
    class Meta:
        """Coach Graphql Query output"""
        node = CoachNode
        interfaces = (TotalCount,)


class CoachAttribute:
    """Coach Input Template """
    first_name = String()
    last_name = String()
    address1 = String()
    address2 = String()
    city = String()
    state = String()
    zip_code = String()
    telephone = String()
    email = String(required=True)
    gender = String()
    sport_id = String()
    grade = String()
    active = Boolean()
    team_id = String()


class CoachInput(InputObjectType, CoachAttribute):
    """Coach Input fields derived from CoachAttribute"""
    sport = InputField(SportsInput)
#    team = InputField(TeamsInput)


class CreateCoach(Mutation):
    """Create Coach Graphql"""
    coach = Field(lambda: CoachNode,
                  description="Coach created by this mutation.")
    ok = Boolean()
    message = List(String)


    class Arguments:
        """Arguments for Create Coach"""
        coach_data = CoachInput(required=True)
        sport_data = SportsInput()
        team_date = TeamsInput()

    def mutate(self, info, coach_data, sport_data=None, team_data=None):
        """Create Coach Graphql"""
        data = input_to_dictionary(coach_data)

        ok = True
        message = []
        coach = None

        try:
            coach = CoachModel(**data)
            coach_db = DB.session.query(CoachModel).filter_by(
                email=data['email']).first()
            if coach_db:
                message.append(f"{data['email']} exists in the database")
                ok = False
                logger.debug(f"Coach: {data['email']} exists in the database")
            else:
                DB.session.add(coach)
                DB.session.commit() 
           
            if sport_data and ok:
                sport = process_sport(sport_data)
                if sport['status']:
                    coach_sport = CoachSport(
                        coach_id=coach.id ,
                        sport_id=sport['sport_id'],
                        active=sport['active'],
                        level_id=sport['level_id'],
                        level_date=sport['level_date']
                    )
                    DB.session.add(coach_sport)
                    DB.session.commit()
                    logger.debug(f"Successfully created, {coach.email}")
                    logger.debug("Successfully created, CoachSport entry")
                    message.append("Successfully Created")
                else:
                    message += sport['message']
                    ok = sport['status']
        except Exception as e:
            ok = False
            message.append("Failed to add coach")
            logger.error(f"Failed to add {coach.email}. error: {e}")
#        if team_data and ok:
#            team_data = input_to_dictionary(team_data)
#            if 'active' not in team_data:
#                team_data['active'] = None
#            if 'join_date' not in data:
#                team_data['join_date'] = None
#            level = DB.session.query(Level).filter_by(id=team_data['level_id']).first()
#            if not level:
#                message.append("Provided Level doesn't exist")
#                ok = False
#                logger.debug(message)
#            sport = DB.session.query(Sport).filter_by(id=data['sport_id']).first()
#            if not sport:
#                message.append("Provided Sport doesn't exists")
#                ok = False
#                logger.debug(message)

        return CreateCoach(coach=coach, ok=ok, message=message)


class UpdateCoach(Mutation):
    """Update Coach Graphql"""
    coach = Field(lambda: CoachNode,
                  description="Coach updated by this mutation.")
    ok = Boolean()
    message = List(String)


    class Arguments:
        """Arguments for Update Coach"""
        coach_data = CoachInput(required=True)
        sport_data = SportsInput()
        team_data = TeamsInput()

    def mutate(self, info, coach_data, sport_data=None, team_data=None):
        """Update Coach Graphql"""
        data = input_to_dictionary(coach_data)
#        team_data = input_to_dictionary(team_data)
        ok = True
        message = []

        try:
            coach = DB.session.query(CoachModel).filter_by(
                email=data['email']).first()
            if coach:
                coach_id = coach.id
                coach = data
                DB.session.commit()
                message.append("Successfully Updated Coach")
                logger.debug("Successfully Updated Coach")
            else:
                message.append(f"Coach with email address, {data['email']} doesn't exist")
                ok = False
            if sport_data and ok:
                sport = process_sport(sport_data)
                if sport['status']:
                    coach_sport = DB.session.query(CoachSport).filter_by(
                        coach_id=coach_id,sport_id=sport['sport_id']).first()
                    if coach_sport:
                        coach_sport.active = sport['active']
                        coach_sport.level_id = sport['level_id'],
                        coach_sport.level_date = sport['level_date']
                    else:
                        coach_sport = CoachSport(
                            coach_id=coach_id,
                            sport_id=sport['sport_id'],
                            active=sport['active'],
                            level_id=sport['level_id'],
                            level_date=sport['level_date']
                        )
                    DB.session.add(coach_sport)                        
                    DB.session.commit()
                else:
                    ok = False
                    message.append("Coach doesn't exist")
        except Exception as e:
            ok = False
            message.append("Error Updating Coach")
            logger.error(f"Error Updating Coach, error: {e}")

        return UpdateCoach(coach=coach, ok=ok, message=message)
