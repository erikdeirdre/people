""" Graphql Parent Schema Module """
import logging
from graphene import (String, Boolean, ID, InputObjectType, List,
                      Field, Mutation, Connection, Node)
from graphene_sqlalchemy import SQLAlchemyObjectType
from app.filters import FilterConnectionField

from helpers.utils import (input_to_dictionary)
from app import DB
from app.database import (Parent as ParentModel, Sport, Team)
from .helpers import (TotalCount, Teams)

logger = logging.getLogger(__name__)


class ParentNode(SQLAlchemyObjectType):
    """ Parent Node """
    class Meta:
        """ Parent Node """
        model = ParentModel
        interfaces = (Node,)
        connection_field_factory = FilterConnectionField.factory

#    sport = List(SportsParents)
#    team = List(Teams)

#    def resolve_sport(self, info):
#        ''' sport resolver '''
#        sports = []
#        results = DB.session.query(ParentSport, Sport).join(Sport).filter(
#            ParentSport.parent_id == self.id)
#        for row in results:
#            description = None
#            active = False
#            if row.ParentSport.active:
#                description = row.Sport.description
#                active = row.ParentSport.active
#
#            sports.append({'id': row.Sport.id,
#                           'description': description,
#                           'active': active})
#        return sports

#    def resolve_team(self, info):
#        ''' team resolver '''
#        teams = []
#        results = DB.session.query(ParentTeam, Team).join(Team).filter(
#            ParentTeam.parent_id == self.id)
#        for row in results:
#            join_date = None
#            join_years = 0
#            active = False
#            description = None
#
#            if row.ParentTeam.active:
#                join_date = row.ParentTeam.join_date
#                join_years = row.ParentTeam.years_active
#                active = row.ParentTeam.active
#                description = row.Team.description
#            teams.append({'id': row.Team.id,
#                           'description': description,
#                           'active': active,
#                           'join_date': join_date,
#                           'join_years': join_years})
#        return teams


class ParentConnection(Connection):
    """Parent Graphql Query output"""
    class Meta:
        """Parent Graphql Query output"""
        node = ParentNode
        interfaces = (TotalCount,)


class ParentAttribute:
    """Parent Input Template """
    first_name = String()
    last_name = String()
    address1 = String()
    address2 = String()
    city = String()
    state = String()
    zip_code = String()
    telephone = String()
    email = String(required=True)
    gender = String()
    sport = String()
    active = Boolean()
    team = String()


class CreateParentInput(InputObjectType, ParentAttribute):
    """Create Parent Input fields derived from ParentAttribute"""


class CreateParent(Mutation):
    """Create Parent Graphql"""
    parent = Field(lambda: ParentNode,
                   description="Parent created by this mutation.")
    ok = Boolean()
    message = String()


    class Arguments:
        """Arguments for Create Parent"""
        parent_data = CreateParentInput(required=True)

    def mutate(self, info, parent_data=None):
        """Create Parent Graphql"""
        data = input_to_dictionary(parent_data)
        ok = True
        message = "Successfully Created"

        try:
            parent = ParentModel(**data)
            parent_db = DB.session.query(ParentModel).filter_by(
                email=data['email']).first()
            if parent_db:
                message = f"{data['email']} exists in the database"
                ok = False
                parent = parent_db
                logger.debug(f"Parent: {data['email']} exists in the database")
            else:
                DB.session.add(parent)
                DB.session.commit()
                logger.debug(f"Successfully created, {parent}")

        except Exception as e:
            ok = False
            message = f"Error creating {parent}"
            logger.error(f"Error creating {parent}. error: {e}")

        return CreateParent(parent=parent, ok=ok, message=message)


class UpdateParentInput(InputObjectType, ParentAttribute):
    """Update Parent Input fields derived from ParentAttribute"""
    id = ID(required=True, description="Global Id of the Parent.")


class UpdateParent(Mutation):
    """Update Parent Graphql"""
    parent = Field(lambda: ParentNode,
                   description="Parent updated by this mutation.")
    ok = Boolean()
    message = String()


    class Arguments:
        """Arguments for Update Parent"""
        parent_data = UpdateParentInput(required=True)

    def mutate(self, info, parent_data):
        """Update Parent Graphql"""
        data = input_to_dictionary(parent_data)
        ok = True
        message = "Successfully Updated"

        try:
            parent = DB.session.query(ParentModel).filter_by(id=data['id']).first()
            parent.update(data)
            DB.session.commit()
            logger.debug(f"Successfully updated, {parent}")

        except Exception as e:
            ok = False
            message = f"Error deleting {parent}"
            logger.error(f"Error deleting {parent}. error: {e}")

        return UpdateParent(parent=parent, ok=ok, message=message)
