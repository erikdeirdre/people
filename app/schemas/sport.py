""" Graphql Sport Schema Module """
import logging
from graphene import (String, Boolean, ID, InputObjectType, Field,
                      Mutation, Interface, Connection, Node)
from graphene_sqlalchemy import SQLAlchemyObjectType
from app.filters import FilterConnectionField

from helpers.utils import (input_to_dictionary)
from app import DB
from app.database import (Sport as SportModel)
from .helpers import TotalCount

logger = logging.getLogger(__name__)


class SportAttribute:
    """Sport Graphql Attributes"""
    description = String()
    active = Boolean()


class SportNode(SQLAlchemyObjectType):
    """Sport Graphql Node output"""
    class Meta:
        """Sport Graphql Node output"""
        model = SportModel
        interfaces = (Node,)
        connection_field_factory = FilterConnectionField.factory


class SportConnection(Connection):
    """Sport Graphql Query output"""
    class Meta:
        """Sport Graphql Query output"""
        node = SportNode
        interfaces = (TotalCount,)


class CreateSportInput(InputObjectType, SportAttribute):
    """Create Sport Input fields derived from SportAttribute"""

"""
mutation AddSport($sport: CreateSportInput!) {
  createSport(sportData: $sport) {
    sport {
      id
      active
    }
  }
}

{ 
  "sport": {
    "description": "Basketball",
    "active": true
  }
}
"""

class CreateSport(Mutation):
    """Sport Graphql Create mutation"""
    sport = Field(lambda: SportNode,
                  description="Sport created by this mutation.")
    ok = Boolean()
    message = String()
    class Arguments:
        """Sport Create Arguments"""
        sport_data = CreateSportInput(required=True)

    def mutate(self, info, sport_data=None):
        """Sport Graphql mutation"""
        ok = True
        message = "Successfully Added"
        data = input_to_dictionary(sport_data)

        try:
            sport = SportModel(**data)
            sport_db = DB.session.query(SportModel).filter_by(
                description=data['description']).first()
            if sport_db:
                message = f"{data['description']} exists in the database"
                ok = False
                sport = sport_db
                logger.debug(f"Sport: {data['description']} exists in the database")
            else:
                DB.session.add(sport)
                DB.session.commit()
                logger.debug(f"Results: {sport}, ok: {ok}, message: {message}")
        except Exception as e:
            ok = False
            message = f"Failed to add {sport}"
            logger.error(f"Failed to add {sport}. error: {e}")

        return CreateSport(sport=sport, ok=ok, message=message)


class UpdateSportInput(InputObjectType, SportAttribute):
    """Sport Graphql Update Input"""
    id = ID(required=True, description="Global Id of the Sport.")

'''
mutation UpdateSport($sport: UpdateSportInput!) {
  updateSport(sportData: $sport) {
    sport {
      id
      active
    }
  }
}

{ 
  "sport": {
    "description": "Basketball",
    "active": false,
    "id": "U3BvcnQ6MTM="
  }
}
'''


class UpdateSport(Mutation):
    """Sport Graphql Update mutation"""
    sport = Field(lambda: SportNode,
                  description="Sport updated by this mutation.")
    ok = Boolean()
    message = String()
    class Arguments:
        """Sport Graphql Update arguments"""
        sport_data = UpdateSportInput(required=True)

    def mutate(self, info, sport_data):
        """Sport Graphql Update mutation"""
        ok = True
        message = "Successfully Updated"
        data = input_to_dictionary(sport_data)

        try:
            sport = DB.session.query(SportModel).filter_by(id=data['id'])
            sport.update(data)
            DB.session.commit()
            logger.debug(f"Successfully updated, {sport}")
        except Exception as e:
            ok = False
            message = f"Error updating {sport}"
            logger.error(f"Error updating {sport}. error: {e}")

        return UpdateSport(sport=sport, ok=ok, message=message)
