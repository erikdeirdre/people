import logging
from sqlalchemy import (and_, or_)

from graphene import (Interface, Int, Boolean, String, ID, ObjectType, Date,
                      InputObjectType, Field, InputField)
from helpers.utils import (input_to_dictionary)
from app import (DB)
from app.database import (Sport, Level, Address as AddressModel)

logger = logging.getLogger(__name__)


class TotalCount(Interface):
    """ Return Row Count """
    total_count = Int()

    def resolve_total_count(self, info):
        """  Return Row Count """
        return self.length


class Address(ObjectType):
    """ Address Graphql Attributes for Fields, Coaches, and Referees"""
    id = ID()
    address1 = String()
    address2 = String()
    city = String()
    state = String()
    zip_code = String()


class Levels(ObjectType):
    """ Levels Graphql Attributes"""
    id = ID()
    description = String()
    active = Boolean()


class Venue(ObjectType):
    """ Venues Graphql Attributes"""
    id = ID()
    name = String()
    address_id = Int()
    address1 = String()
    address2 = String()
    city = String()
    state = String()
    zip_code = String()
    active = Boolean()


class Sports(ObjectType):
    """ Sports Graphql Attributes for Referees and Coaches"""
    id = ID()
    description = String()
    active = Boolean()
    level = String()
    level_date = Date()
    level_years = Int()
    level_id = Int()


class SportsPlayers(ObjectType):
    """ Sports Graphql Attributes for Players"""
    id = ID()
    description = String()
    active = Boolean()


class Teams(ObjectType):
    """ Teams Graphql Attributes"""
    id = ID()
    description = String()
    active = Boolean()
    join_date = Date()
    join_years = Int()


class SportsAttribute:
    """Sport Attribute for Coach and Referees"""
    description = String()
    active = Boolean()
    level_id = ID()
    level_date = Date()
    sport_id = ID()


class TeamsAttribute:
    """Team Attribute for Coach and Players"""
    description = String()
    active = Boolean()
    join_date = Date()
    join_years = Int()


class AddressAttribute:
    address1 = String()
    address2 = String()
    city = String()
    state = String()
    zip_code = String()


class VenueAttribute:
    """Venue Input Template """
    name = String()
    address1 = String()
    address2 = String()
    city = String()
    state = String()
    zip_code = String()
    active = Boolean()


class SportsInput(InputObjectType, SportsAttribute):
    """Sports Input fields derived from SportsAttribute"""


class TeamsInput(InputObjectType, TeamsAttribute):
    """Teams Input fields derived from TeamsAttribute"""


class AddressInput(InputObjectType, AddressAttribute):
    """Address Input fields derived from AddressAttribute"""


class VenueAttribute:
    """Venue Input Template """
    name = String()
    address = Field(Address)
    active = Boolean()


class VenueInput(InputObjectType, VenueAttribute):
    """Venue Input fields derived from VenueAttribute"""
    address = InputField(AddressInput, required=True)


def process_sport(sport_data):
    message = []
    status = True
    data = input_to_dictionary(sport_data)
    if 'active' not in data:
        data['active'] = None
    if 'level_date' not in data:
        data['level_date'] = None
    level = DB.session.query(Level).filter_by(id=data['level_id']).first()
    if not level:
        message.append("Provided Level doesn't exist")
        status = False
        logger.debug(message)
    sport = DB.session.query(Sport).filter_by(id=data['sport_id']).first()
    if not sport:
        message.append("Provided Sport doesn't exist")
        status = False
        logger.debug(message)

    return {
        "status": status,
        "sport_id": data['sport_id'],
        "active": data['active'],
        "level_id": data['level_id'],
        "level_date": data['level_date'],
        "message": message
    } 

def process_address(address_data):
    return_value = {
        "id": None,
        "message": [],
        "status": True
    }

    try:
        address_db = DB.session.query(AddressModel).filter_by(
            address1=address_data['address1'],
            city=address_data['city'],
            state=address_data['state'],
        ).first()
        if address_db:
            return_value["id"] = address_db.id 
        else:
            address_db = DB.session.query(AddressModel).filter_by(
                    address1=address_data['address1'],
                    zip_code=address_data['zip_code']
                ).first()      
            if address_db:
                return_value["id"] = address_db.id 
            else: 
                address_db = AddressModel(
                    address1=address_data['address1']  if 'address1' in address_data else None,
                    address2= address_data['address2'] if 'address2' in address_data else None,
                    city=address_data['city'] if 'city' in address_data else None,
                    state=address_data['state'] if 'state' in address_data else None,
                    zip_code=address_data['zip_code'] if 'zip_code' in address_data else None
                )
                DB.session.add(address_db)                  
                DB.session.commit()
                DB.session.refresh(address_db)
                return_value["id"] = address_db.id            
    except Exception as e:
        return_value["status"] = False
        return_value["message"].append(f"Failed to add address, {address_data.address1}")
        logger.error(f"Failed to add {address_data.address1}. error: {e}")

    return return_value