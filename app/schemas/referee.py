""" Graphql Referee Schema Module """
import logging
from graphene import (String, Boolean, ID, InputObjectType, Field,
                      Mutation, Connection, Node, List, InputField)
from graphene_sqlalchemy import SQLAlchemyObjectType
from app.filters import FilterConnectionField

from helpers.utils import (input_to_dictionary)
from app import (DB)
from app.database import (Referee as RefereeModel, Sport, RefereeSport,
                          Level)
from .helpers import (TotalCount, Sports, SportsInput, process_sport)

logger = logging.getLogger(__name__)


class RefereeAttribute:
    """Referee Graphql Attributes"""
    first_name = String()
    last_name = String()
    address1 = String()
    address2 = String()
    city = String()
    state = String()
    zip_code = String()
    telephone = String()
    email = String(required=True)
    gender = String()
    sport_id = String()
    active = Boolean()


class RefereeNode(SQLAlchemyObjectType):
    """Referee Node """
    class Meta:
        """ Referee Node """
        model = RefereeModel
        interfaces = (Node,)
        connection_field_factory = FilterConnectionField.factory

    sport = List(Sports)


    def resolve_sport(self, info):
        ''' sport resolver '''
        sports = []
        results = DB.session.query(RefereeSport, Sport).join(Sport).filter(
            RefereeSport.referee_id == self.id)
        for row in results:
            level = None
            level_date = None
            level_years = 0
            description = None
            active = False
            level_id = None
            sport_id = None
            if row.RefereeSport.level:
                level = row.RefereeSport.level.description
                level_date = row.RefereeSport.level_date
                level_years = row.RefereeSport.years_level
                description = row.Sport.description
                active = row.RefereeSport.active
                level_id = row.RefereeSport.level_id
                sport_id = row.RefereeSport.sport_id

            sports.append({'id': sport_id,
                           'description': description,
                           'active': active,
                           'level_id': level_id,
                           'level': level,
                           'level_date': level_date,
                           'level_years': level_years})

        return sports


class RefereeConnection(Connection):
    """ Referee Connection """
    class Meta:
        """ Referee Connection """
        node = RefereeNode
        interfaces = (TotalCount,)


class RefereeInput(InputObjectType, RefereeAttribute):
    """Create Referee Input fields derived from RefereeAttribute"""

    sport = InputField(SportsInput)


class CreateReferee(Mutation):
    """Create Referee Graphql"""
    referee = Field(lambda: RefereeNode,
                    description="Referee created by this mutation.")
    ok = Boolean()
    message = List(String)


    class Arguments:
        """Create Referee Arguments"""
        referee_data = RefereeInput(required=True)
        sport_data = SportsInput()

    def mutate(self, info, referee_data, sport_data=None):
        """Create Referee Graphql"""
        ref_data = input_to_dictionary(referee_data)
        ok = True
        message = []
        referee = None

        try:
            referee = RefereeModel(**ref_data)

            referee_db = DB.session.query(RefereeModel).filter_by(
                email=ref_data['email']).first()
            if referee_db:
                message.append(f"{ref_data['email']} exists in the database")
                ok = False
                referee = referee_db
                logger.debug(f"Referee: {ref_data['email']} exists in the database")
            else:
                DB.session.add(referee)
                DB.session.commit()
            if sport_data and ok:
                sport = process_sport(sport_data)
                if sport['status']:
                    referee_sport = RefereeSport(
                        referee_id=referee.id ,
                        sport_id=sport['sport_id'],
                        active=sport['active'],
                        level_id=sport['level_id'],
                        level_date=sport['level_date']
                    )
                    DB.session.add(referee_sport)
                    DB.session.commit()
                    logger.debug(f"Successfully created, {referee.email}")
                    logger.debug("Successfully created, RefereeSport entry")
                    message.append("Successfully Created")
                else:
                    message += sport['message']
                    ok = sport['status']
            else:
                ok = False
                message.append("Referee requires sport information")
        except Exception as e:
            ok = False
            message.append("Failed to add referee")
            logger.error(f"Failed to add {referee.email}. error: {e}")
        return CreateReferee(referee=referee, ok=ok, message=message)


class UpdateReferee(Mutation):
    """Update Referee Graphql"""
    referee = Field(lambda: RefereeNode,
                    description="Referee updated by this mutation.")
    ok = Boolean()
    message = List(String)


    class Arguments:
        """Arguments for Update Referee"""
        referee_data = RefereeInput(required=True)
        sport_data = SportsInput()

    def mutate(self, info, referee_data, sport_data=None):
        """Update Referee Graphql"""
        data = input_to_dictionary(referee_data)
        ok = True
        message = []

        try:
            referee = DB.session.query(RefereeModel).filter_by(email=data['email']).first()
            if referee:
                referee_id = referee.id
                referee = data
                DB.session.commit()
                logger.debug("Successfully Updated referee")
                message.append("Successfully Updated Referee")
            else:
                message.append(f"Referee with email address, {data['email']} doesn't exist")
                ok = False
            if sport_data and ok:
                sport = process_sport(sport_data)
                if sport['status']:
                    referee_sport = DB.session.query(RefereeSport).filter_by(
                        referee_id=referee_id,sport_id=sport['sport_id']).first()
                    if referee_sport:
                        referee_sport.active = sport['active']
                        referee_sport.level_id = sport['level_id'],
                        referee_sport.level_date = sport['level_date']
                    else:
                        referee_sport = RefereeSport(
                            referee_id=referee_id ,
                            sport_id=sport['sport_id'],
                            active=sport['active'],
                            level_id=sport['level_id'],
                            level_date=sport['level_date']
                        )
                        DB.session.add(referee_sport)                        
                    DB.session.commit()
                    logger.debug("Successfully updated, RefereeSport entry")
                    message.append("Successfully Updated RefereeSport")
                else:
                    message += sport['message']
                    ok = sport['status']                    
        except Exception as e:
            ok = False
            message.append(f"Failed to add {referee.email}")
            logger.error(f"Failed to add {referee.email}. error: {e}")

        return UpdateReferee(referee=referee, ok=ok, message=message)
