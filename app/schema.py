""" Graphql Schema Module """
from graphene import (ObjectType, String, Field, relay, Schema, Argument,
                      Mutation)
from graphene_sqlalchemy_filter import FilterableConnectionField

from .filters import (FilterConnectionField, LevelFilter)
from .schemas.team import (TeamNode, TeamConnection, CreateTeam, UpdateTeam)
from .schemas.sport import (SportNode, SportConnection, CreateSport,
                            UpdateSport)
from .schemas.referee import (RefereeNode, RefereeConnection, CreateReferee,
                              UpdateReferee)
from .schemas.coach import (CoachNode, CoachConnection, CreateCoach,
                            UpdateCoach)
from .schemas.player import (PlayerNode, PlayerConnection, CreatePlayer,
                             UpdatePlayer)
from .schemas.parent import (ParentNode, ParentConnection, CreateParent,
                             UpdateParent)
from .schemas.address import (resolve_city_states, resolve_address,
                              CityStateNode, AddressNode)
from .schemas.level import (LevelConnection, LevelNode)
from .schemas.venue import (CreateVenue, VenueNode, VenueConnection, UpdateVenue)
from .schemas.game import (GameNode, GameConnection, CreateGame, UpdateGame)


class Query(ObjectType):
    """Create Query object list"""
    node = relay.Node.Field()

    city_state = Field(
        CityStateNode,
        postalcode=String(required=True),
        resolver=resolve_city_states
    )

    address = Field(
        AddressNode,
        postalcode=Argument(String, required=True),
        address1=Argument(String, required=True),
        address2=Argument(String),
        city=Argument(String),
        state=Argument(String),
        resolver=resolve_address
    )

 #   address = relay.Node.Field(AddressNode)
 #   citystate = relay.Node.Field(CityStateNode)
    sport = relay.Node.Field(SportNode)
    team = relay.Node.Field(TeamNode)
    referee = relay.Node.Field(RefereeNode)
    coach = relay.Node.Field(CoachNode)
    player = relay.Node.Field(PlayerNode)
    parent = relay.Node.Field(ParentNode)
    level = relay.Node.Field(LevelNode)
    venue = relay.Node.Field(VenueNode)
    game = relay.Node.Field(GameNode)

    all_teams = FilterConnectionField(TeamConnection)
    all_sports = FilterConnectionField(SportConnection)
    all_referees = FilterConnectionField(RefereeConnection)
    all_coaches = FilterConnectionField(CoachConnection)
    all_players = FilterConnectionField(PlayerConnection)
    all_parents = FilterConnectionField(ParentConnection)
    all_levels = FilterableConnectionField(LevelConnection,
                                           filters=LevelFilter())
    all_venues = FilterConnectionField(VenueConnection)
    all_games = FilterConnectionField(GameConnection)


class Mutation(ObjectType):
    """Create Mutation object list"""
    createReferee = CreateReferee.Field()
    createCoach = CreateCoach.Field()
    createSport = CreateSport.Field()
    createTeam = CreateTeam.Field()
    createPlayer = CreatePlayer.Field()
    createParent = CreateParent.Field()
    createVenue = CreateVenue.Field()
    createGame = CreateGame.Field()

    updateTeam = UpdateTeam.Field()
    updateReferee = UpdateReferee.Field()
    updateCoach = UpdateCoach.Field()
    updateSport = UpdateSport.Field()
    updatePlayer = UpdatePlayer.Field()
    updateParent = UpdateParent.Field()
    updateVenue = UpdateVenue.Field()
    updateGame = UpdateGame.Field()


SCHEMA = Schema(query=Query, mutation=Mutation)
